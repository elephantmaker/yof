<?php

declare(strict_types=1);

namespace App\Tests;

use DMS\PHPUnitExtensions\ArraySubset\Assert as ArraySubsetAsserts;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final readonly class ResponseForTests
{
    public function __construct(
        private Response $response,
    ) {
    }

    public function assertContentEquals(?array $expected = null): self
    {
        $actual = json_decode($this->response->getContent(), true);
        Assert::assertEquals($expected, $actual);

        return $this;
    }

    public function assertContentContains(?array $expected = null): self
    {
        $actual = json_decode($this->response->getContent(), true);
        ArraySubsetAsserts::assertArraySubset($expected, $actual);

        return $this;
    }

    public function assertOk200(): self
    {
        return $this->assertResponseCodeIsEquals(Response::HTTP_OK);
    }

    public function assertBadRequest400(): self
    {
        return $this->assertResponseCodeIsEquals(Response::HTTP_BAD_REQUEST);
    }

    public function assertUnauthorized401(): self
    {
        return $this->assertResponseCodeIsEquals(Response::HTTP_UNAUTHORIZED);
    }

    public function assertForbidden403(): self
    {
        return $this->assertResponseCodeIsEquals(Response::HTTP_FORBIDDEN);
    }

    public function assertNotFound404(): self
    {
        return $this->assertResponseCodeIsEquals(Response::HTTP_NOT_FOUND);
    }

    public function assertInternalServerError500(): self
    {
        return $this->assertResponseCodeIsEquals(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    private function assertResponseCodeIsEquals(int $code): self
    {
        Assert::assertEquals($code, $this->response->getStatusCode());

        return $this;
    }
}
