<?php

declare(strict_types=1);

namespace App\Tests;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;

abstract class FunctionalTestCase extends WebTestCase
{
    protected Faker $faker;

    protected EntityManager $entityManager;

    protected function setUp(): void
    {
        parent::setUp();
        parent::createClient();

        $this->entityManager = parent::getContainer()->get(EntityManagerInterface::class);
        $this->faker = new Faker();

        $this->startTransaction();
    }

    protected function tearDown(): void
    {
        $this->rollbackTransaction();

        parent::tearDown();
    }

    protected function runRequest(
        string $uri,
        string $method,
        array $parameters = [],
        ?array $content = null,
    ): ResponseForTests {
        /** @var KernelBrowser $client */
        $client = parent::getClient();

        if ($content != null) {
            $client->jsonRequest($method, $uri, $content);
        } else {
            $client->request($method, $uri, $parameters);
        }

        return new ResponseForTests($client->getResponse());
    }

    protected function runCommand(string $name, array $arguments = []): void
    {
        $application = new Application(parent::$kernel);

        $command = $application->find($name);
        $commandTester = new CommandTester($command);
        $commandTester->execute($arguments);
    }

    /**
     * @psalm-template RealInstanceType of object
     * @psalm-param class-string<RealInstanceType> $serviceName
     * @psalm-return RealInstanceType
     */
    protected function getService(string $serviceName)
    {
        return parent::getContainer()->get($serviceName);
    }

    /**
     * @psalm-template RealInstanceType of object
     * @psalm-param class-string<RealInstanceType> $serviceName
     * @psalm-return RealInstanceType
     */
    protected function mockService(string $serviceName): MockObject
    {
        $mock = $this
            ->getMockBuilder($serviceName)
            ->disableOriginalConstructor()
            ->disableAutoReturnValueGeneration()
            ->getMock();

        parent::getContainer()->set($serviceName, $mock);

        return $mock;
    }

    private function startTransaction(): void
    {
        $this->entityManager->getConnection()->setNestTransactionsWithSavepoints(true);
        $this->entityManager->beginTransaction();
    }

    private function rollbackTransaction(): void
    {
        $this->entityManager->rollback();
        $this->entityManager->close();
    }
}
