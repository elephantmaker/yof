<?php

declare(strict_types=1);

namespace App\Tests\Action\Public\Blog;

use App\Tests\FunctionalTestCase;

final class AddTest extends FunctionalTestCase
{
    public function testAddBlog(): void
    {
        $content = [
            'title' => $this->faker->string(),
            'text' => $this->faker->json(),
            'tags' => [
                $this->faker->uuid(),
            ]
        ];

        $this
            ->runRequest('/api/public/v1/blog', 'POST', content: $content)
            ->assertOk200()
            ->assertContentEquals(['success' => true]);
    }
}
