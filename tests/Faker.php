<?php

declare(strict_types=1);

namespace App\Tests;

use Faker\Factory;
use Faker\Generator;

final readonly class Faker
{
    private Generator $generator;

    public function __construct()
    {
        $this->generator = Factory::create();
    }

    public function intPositive(): int
    {
        return $this->generator->unique()->numberBetween(1);
    }

    public function intOrNull(): ?int
    {
        return $this->boolean() ? $this->intPositive() : null;
    }

    public function boolean(): bool
    {
        return $this->generator->boolean();
    }

    public function booleanOrNull(): ?bool
    {
        return $this->boolean() ? $this->boolean() : null;
    }

    public function string(): string
    {
        return $this->generator->unique()->word();
    }

    public function stringOrNull(): ?string
    {
        return $this->boolean() ? $this->string() : null;
    }

    public function json(): string
    {
        return $this->string();
    }

    public function uuid(): string
    {
        return $this->generator->uuid();
    }
}
