<?php

declare(strict_types=1);

namespace App;

use App\Framework\ItIs;

#[ItIs\Entity]
final class DeleteReason
{
    public DeleteReason\Id $id;
    public DeleteReason\Text $text;
    public DeleteReason\IsDefault $isDefault;
}
