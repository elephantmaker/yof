<?php

declare(strict_types=1);

namespace App\Framework\ItIs;

use Attribute;

#[Attribute(Attribute::TARGET_PARAMETER)]
final readonly class Service
{
}
