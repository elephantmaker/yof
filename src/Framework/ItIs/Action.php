<?php

declare(strict_types=1);

namespace App\Framework\ItIs;

use Attribute;
use Symfony\Component\Routing\Attribute\Route;

#[Attribute(Attribute::TARGET_METHOD)]
class Action extends Route
{
    public function __construct(
        public string $url,
        public string $method,
        public string $description,
    ) {
        parent::__construct($url, methods: $this->method);
    }
}
