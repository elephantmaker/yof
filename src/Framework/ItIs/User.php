<?php

declare(strict_types=1);

namespace App\Framework\ItIs;

use App\CommonBundle\Role;
use Attribute;

#[Attribute(Attribute::TARGET_PARAMETER)]
final readonly class User
{
    /** @var Role[] */
    public array $roles;

    public function __construct(
        Role ...$roles
    ) {
        $this->roles = $roles;
    }
}
