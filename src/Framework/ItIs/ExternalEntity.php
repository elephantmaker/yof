<?php

declare(strict_types=1);

namespace App\Framework\ItIs;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
final readonly class ExternalEntity
{
}
