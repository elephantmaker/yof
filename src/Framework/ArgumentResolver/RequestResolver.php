<?php

declare(strict_types=1);

namespace App\Framework\ArgumentResolver;

use App\Framework\ItIs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

final readonly class RequestResolver implements ValueResolverInterface
{
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        if (!$argument->getAttributesOfType(ItIs\Request::class)) {
            return [];
        }

        $dtoClass = $argument->getType();
        $dtoReflection = new \ReflectionClass($dtoClass);

        $constructorArgs = [];
        $decodedContent = json_decode($request->getContent(), true);

        foreach ($dtoReflection->getConstructor()->getParameters() as $parameter) {
            $value = $decodedContent[$parameter->getName()];

            $class = $parameter->getType()->getName();
            $constructorArgs[] = new $class($value);
        };

        return [
            new $dtoClass(...$constructorArgs)
        ];
    }
}
