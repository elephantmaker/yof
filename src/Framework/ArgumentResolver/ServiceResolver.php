<?php

declare(strict_types=1);

namespace App\Framework\ArgumentResolver;

use App\Framework\ItIs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\HttpKernel\KernelInterface;

final readonly class ServiceResolver implements ValueResolverInterface
{
    public function __construct(
        private KernelInterface $kernel,
    ) {
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        if (!$argument->getAttributesOfType(ItIs\Service::class)) {
            return [];
        }

        return [$this->kernel->getContainer()->get($argument->getType())];
    }
}
