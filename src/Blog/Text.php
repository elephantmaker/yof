<?php

declare(strict_types=1);

namespace App\Blog;

final class Text
{
    public function __construct(
        public string $value,
    ) {
    }
}
