<?php

declare(strict_types=1);

namespace App\Blog;

final readonly class Tags
{
    public function __construct(
        /** @var string[] $values */
        private array $values,
    ) {
    }
}
