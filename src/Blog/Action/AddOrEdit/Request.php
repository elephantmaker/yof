<?php

declare(strict_types=1);

namespace App\Blog\Action\AddOrEdit;

use App\Blog;

final readonly class Request
{
    public function __construct(
        public Blog\Title $title,
        public Blog\Text $text,
        public Blog\Tags $tags,
    ) {
    }
}
