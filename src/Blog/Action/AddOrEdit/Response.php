<?php

declare(strict_types=1);

namespace App\Blog\Action\AddOrEdit;

use App\Author;
use App\Blog;

final readonly class Response
{
    public function __construct(
        public Author $author,
        public Blog\Title $title,
        public Blog\Text $text,
        public Blog\Tags $tags,
        public Blog\Uuid $uuid,
        public Blog\CreatedAt $createdAt,
        public Blog\UpdatedAt $updatedAt,
        public Blog\DeleteReason $deleteReason,
    ) {
    }
}
