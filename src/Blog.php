<?php

declare(strict_types=1);

namespace App;

use App\Blog\Action;
use App\CommonBundle\Role;
use App\CommonBundle\UserInterface;
use App\Framework\ItIs;

#[ItIs\Entity]
final class Blog
{
    public function __construct(
        private readonly Author $author,
        private Blog\Title $title,
        private Blog\Text $text,
        private Blog\Tags $tags,
        private readonly Blog\Uuid $uuid = new Blog\Uuid(),
        private readonly Blog\CreatedAt $createdAt = new Blog\CreatedAt(),
        private Blog\UpdatedAt $updatedAt = new Blog\UpdatedAt(),
        private Blog\DeleteReason $deleteReason = new Blog\DeleteReason(),
    ) {
    }

    #[ItIs\Action('/api/public/v1/blog', 'POST', 'Создание блога')]
    public static function add(
        #[ItIs\Request] Action\AddOrEdit\Request $request,
        #[ItIs\User(Role::Admin, Role::User)] UserInterface $user,
        #[ItIs\Service] Blog\Service\Repository $repository,
    ): Action\AddOrEdit\Response {
        $author = new Author($user->getId());

        $blog = new Blog(
            $author,
            $request->title,
            $request->text,
            $request->tags,
        );

        $repository->add($blog, true);

        return new Action\AddOrEdit\Response(
            $blog->author,
            $blog->title,
            $blog->text,
            $blog->tags,
            $blog->uuid,
            $blog->createdAt,
            $blog->updatedAt,
            $blog->deleteReason,
        );
    }

    #[ItIs\Action('/api/public/v1/blog/{blog_uuid}', 'PUT', 'Редактирование блога')]
    public function edit(
        #[ItIs\Request] Action\AddOrEdit\Request $request,
        #[ItIs\User(Role::Admin, Role::User)] UserInterface $user,
        #[ItIs\Service] Blog\Service\Repository $repository,
    ): Action\AddOrEdit\Response {
        $this->title = $request->title;
        $this->text = $request->text;
        $this->tags = $request->tags;
        $this->updatedAt->setNow();

        $repository->add($this, true);

        return new Action\AddOrEdit\Response(
            $this->author,
            $this->title,
            $this->text,
            $this->tags,
            $this->uuid,
            $this->createdAt,
            $this->updatedAt,
            $this->deleteReason,
        );
    }


//    #[ItIs\Action('/api/public/v1/blog', 'POST', 'Создание блога')]
//    public static function add(
//        #[ItIs\Request] Action\Add\Request $request,
////        #[ItIs\User(Role::Admin, Role::User)] UserInterface $user,
//        #[ItIs\Service] Blog\Repository $repository,
//    ): JsonResponse {
//        return new JsonResponse(['success' => true]);
//    }
}
