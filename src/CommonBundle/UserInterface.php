<?php

namespace App\CommonBundle;

interface UserInterface
{
    public function getId(): Entity\User\Id;
}