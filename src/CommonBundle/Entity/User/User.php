<?php

declare(strict_types=1);

namespace App\CommonBundle\Entity\User;

final readonly class User
{
    public function __construct(
        public Id $id,
        public Profile $profile,
    ) {
    }
}
