<?php

declare(strict_types=1);

namespace App\CommonBundle\Entity\User;

final readonly class Profile
{
    public function __construct(
        public Nickname $nickname,
    ) {
    }
}
