<?php

declare(strict_types=1);

namespace App\CommonBundle;

enum Role: string
{
    case Admin = 'admin';
    case User = 'user';
}
