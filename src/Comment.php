<?php

declare(strict_types=1);

namespace App;

use App\Framework\ItIs;

#[ItIs\Entity]
final class Comment
{
    public Comment\Uuid $uuid;
    public Blog $blog;
    public Comment\ParentComment $parent;
    public Author $author;
    public Comment\Text $text;
    public Comment\IsAccepted $isAccepted;
    public Comment\CreatedAt $createdAt;
    public Comment\UpdatedAt $updatedAt;
    public Comment\DeleteReason $deleteReason;
}
