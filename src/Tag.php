<?php

declare(strict_types=1);

namespace App;

use App\Framework\ItIs;

#[ItIs\Entity]
final class Tag
{
    public Tag\Uuid $uuid;
    public Tag\Value $value;
    public Tag\CreatedAt $createdAt;
    public Tag\UpdatedAt $updatedAt;
    public Tag\DeletedAt $deletedAt;
    public Tag\Blogs $blogs;
}
