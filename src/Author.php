<?php

declare(strict_types=1);

namespace App;

use App\CommonBundle\Entity\User;
use App\Framework\ItIs;

#[ItIs\ExternalEntity]
final readonly class Author
{
    public function __construct(
        public User\Id $id,
        public User\Nickname $nickname = new User\Nickname(),
    ) {
    }
}
