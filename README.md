# Установка

1. Установить make
2. Установить docker
3. Установить docker-compose
4. Выполнить команду
```bash
make up
```

# Xdebug для PhpStorm

* Settings -> PHP -> CLI Interpreter -> ... -> Add -> From Docker, Vagrant, VM, WSL, Remote... -> Docker Compose
   * Service: `yof-php`
* Settings -> PHP -> CLI Interpreter -> ... -> Lifecycle -> Connect to existing container (docker-compose exec)
* Settings -> PHP -> Servers -> Add
  * Name: `Docker`
  * Host: `localhost`
  * Port: `80`
  * Выбрать "Use path mappings" и в Project files вписать `/var/www`

# Тесты

Перед запуском тестов выполнить
```bash
make init-tests
```

# Query builder

Описание функционала оформлено в виде тест-кейсов: `App\Tests\Functional\QueryParserTest`
