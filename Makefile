up:
	docker network create -d bridge yof-local || true
	docker-compose up -d

down:
	docker-compose down

exec:
	docker-compose exec yof-php /bin/sh

ps:
	docker-compose ps

init:
	docker-compose exec yof-php composer install
	docker-compose exec yof-php bin/console doctrine:database:create || true
	docker-compose exec yof-php bin/console doctrine:migrations:migrate --no-interaction || true

init-tests:
	docker-compose exec yof-php composer install
	docker-compose exec yof-php bin/console doctrine:database:create --env=test || true
	docker-compose exec yof-php bin/console doctrine:migrations:migrate --no-interaction --env=test || true
